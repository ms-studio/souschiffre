<?php
/**
 * souschiffre functions and definitions
 *
 * @package souschiffre
 */
 
 
 // Custom Functions for CSS/Javascript Versioning
 $GLOBALS["TEMPLATE_URL"] = get_bloginfo('template_url')."/";
 $GLOBALS["TEMPLATE_RELATIVE_URL"] = wp_make_link_relative($GLOBALS["TEMPLATE_URL"]);
 
 
 // Custom image sizes
 // ****************************** 
  
  if ( function_exists( 'add_image_size' ) ) { 
  	//add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
  	// thumbnail: 150, 150, true
  	add_image_size( 'standard', 492, 984 );
  	// gutter = 20px
  	// 492-20 = 472, 472/2=236
  	add_image_size( 'horiz-thumb', 236, 177, true ); // format 4/3
//  	add_image_size( 'vertical-thumb', 177, 236, true );
  	// medium: 300, 300
  	// large: 1024, 1024
//  	add_image_size( 'superwide', 1280, 196, true );
  	//add_image_size( 'landscape', 304, 184, true ); // true = cropped
  }
 
 /*
  * custom taxonomies
  ********************
  * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
 
 
 //create two taxonomies, genres and writers for the post type "book"
 function souschiffre_taxonomies() 
 {
   register_taxonomy('lieux','post',array( 
    		'hierarchical' => false, 
    		'label' => 'Lieux',
    		'labels'  => array(
    			'name'                => _x( 'Lieux', 'taxonomy general name' ),
    			'singular_name'       => _x( 'Lieu', 'taxonomy singular name' ),
    			'search_items'        => __( 'Chercher dans les lieux' ),
    			'menu_name'           => __( 'Lieux' )
    		),
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array('slug' => 'lieux'),
    		'singular_label' => 'Lieu') 
    );
   	 
   register_taxonomy('reglages','post',array( 
   	 		'hierarchical' => true, 
   	 		'label' => 'Réglages',
   	 		'labels'  => array(
   	 			'name'                => _x( 'Réglages', 'taxonomy general name' ),
   	 			'singular_name'       => _x( 'Réglage', 'taxonomy singular name' ),
   	 			'search_items'        => __( 'Chercher dans les réglages' ),
   	 			'menu_name'           => __( 'Réglages' )
   	 		),
   	 		'show_ui' => true,
   	 		'query_var' => true,
   	 		'rewrite' => array('slug' => 'reglages'),
   	 		'singular_label' => 'Réglage') 
   	 );
   
 } // end souschiffre_taxonomies()  function
  //hook into the init action and call souschiffre_taxonomies() when it fires
  add_action( 'init', 'souschiffre_taxonomies', 0 );
 
/*
 * Posts 2 Posts
 ***************
*/
function souschiffre_p2p() {

// Make sure the Posts 2 Posts plugin is active.
if ( !function_exists( 'p2p_register_connection_type' ) )
	return;
	
p2p_register_connection_type( array( 
			'name' => 'projects',
			'from' => 'post',
			'to' => 'post',
			'reciprocal' => true,
			'title' => 'Connexion'
		) );
}
add_action( 'p2p_init', 'souschiffre_p2p' );
	 
/*
 * Image Array
 *****************
*/
 
 
function img_toolbox_array($size = 'thumbnail', $howmany = -1) {
	if ( $images = get_children ( array (
		'post_parent'    => get_the_ID(),
		'post_type'      => 'attachment',
		'numberposts'    => $howmany , // -1 = show all
		'post_status'    => null,
		'post_mime_type' => 'image',
		'orderby' => 'menu_order',
		'order' => 'ASC',
	))) {
		$img_gallery_array = array();
		foreach ( $images as $image ) {
			
			$img_id = $image->ID;
			$img_custom_size = image_downsize( $img_id, $size );
			$img_large_size = wp_get_attachment_image_src( $img_id, 'large' );
			$img_full_size = wp_get_attachment_image_src( $img_id, 'full' );
			$img_horiz_thumb = wp_get_attachment_image_src( $img_id, 'horiz-thumb' );
			
			$img_gallery_array[] = array( 
					"id" => $img_id,
						// URLs 
			    	"url-custom" => $img_custom_size[0],
			    	"url-large" => $img_large_size[0],
			    	"url-full" => $img_full_size[0],
			    	"url-horiz-thumb" => $img_horiz_thumb[0],
			    	// dimensions
			    	"width-custom" => $img_custom_size[1],
			    	"height-custom" => $img_custom_size[2],
			    	// defaults
			    	"width-large" => $img_large_size[1],
			    	"height-large" => $img_large_size[2],
			    	"width-full" => $img_full_size[1],
			    	"height-full" => $img_full_size[2],
			    	// text
			    	"title" => apply_filters('the_title',$image->post_title),
// error in 3.6-alpha
// https://gist.github.com/ms-studio/3928765#comment-824110
			    	"caption" => $image->post_excerpt,
			    	"description" => $image->post_content,
			    	"alt-text" => get_post_meta($img_id, '_wp_attachment_image_alt', true),
			   );
		} // end foreach
		return $img_gallery_array;
	} 
}
 
 
 