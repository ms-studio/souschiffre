<?php
/**
 * @package souschiffre
 *
 * NOTE: 
 *
 * The contents are defined in :
 * archive-array.php
 * 
 */
 
 if ($item['status'] == "sub-item" ) {
 
 	// do noting - we don't show sub-items
 
 } else {
 
?>

<article id="post-<?php echo $item['id']; ?>" class="archive-<?php echo $item['status']; ?> item archive-item">

	<a href="<?php echo $item['url']; ?>" class="dblock unstyled">
	<header class="entry-header">
		<h1 class="entry-title"><span class="hoverable"><?php echo $item['title']; ?></span></h1>
		
			<?php 
			
			if ( '' != $item['pubdate-human'] ) {
			
				echo '<div class="entry-meta colorable">';
				echo $item['pubdate-human'];
				echo '</div>';
				
			}
			
			?>
	</header><!-- .entry-header -->

<?php
		if ( '' != $item['image-url'] ) {
			echo '<img class="archive-item-img" src="'.$item['image-url'].'"/>';
		}
	?>
	</a>
</article><!-- #post-## -->

<?php

}

?>