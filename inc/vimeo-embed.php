<?php 

// testing for the vimeo ID: occurring in inc/meta-link-vimeo.php

// Turn off all error reporting
	error_reporting(0);
	
// for Infomaniak... :-(
// http://php.net/manual/en/function.error-reporting.php

		if ( empty($vimeo_howmany) ) {
		
			$vimeo_howmany = 10;
		
		}
		
//		echo $vimeo_type;
		
		if ($vimeo_type == 'channel') {
		
			$api_endpoint = 'http://vimeo.com/api/v2/channel/'.$vimeo_id.'/videos.xml';
			
		} else if ($vimeo_type == 'album') {
		
			$api_endpoint = 'http://vimeo.com/api/v2/album/'.$vimeo_id.'/videos.xml';
			
		} else { // single video item
		
			$api_endpoint = 'http://vimeo.com/api/v2/video/'.$vimeo_id.'.xml';
		
		}
		
		// note: we are not using Vimeo Groups currently.
			
		/* Vimeo API Docs:
		
		// http://developer.vimeo.com/apis/simple
		
		// https://github.com/vimeo/vimeo-api-examples/blob/master/simple-api/simple/simple.php
			
		// the simplexml_load_string() php function:
		// http://php.net/manual/en/function.simplexml-load-string.php

		// ******************************************************/
		
		// ERROR HANDLING
		// $use_errors = libxml_use_internal_errors(true);
		// information on erro handling:
		// http://stackoverflow.com/questions/1307275/simplexml-error-handling-php
		// more info: http://php.net/manual/en/function.libxml-use-internal-errors.php

  //   libxml_clear_errors();
		 
		 $videos = simplexml_load_string(curl_get($api_endpoint));
		 
		 if (!$videos) {
		   echo "Erreur de chargement";
		 }
		    
		    $vimcounter = 0;
		    
		    // test if the array exists 
		    if (!empty($videos)) {
		    
		    // NOW RUN THE LOOP
		    
		    ?><section class="vimeo-gallery">
		    
		      <div class="vimeo-gallery-inside"><?
		    
		    			foreach ($videos->video as $video) { 
		    			
		    			if(++$vimcounter > $vimeo_howmany) break; // maximum of 5 results
		    			
		    				// opening tags
		    				// *****************
		    				
		    				$vimeo_img_large = $video->thumbnail_large;
		    				$vimeo_width = $video->width;
		    				$vimeo_height = $video->height;
		    				$vimeo_title = $video->title;
		    				
		    				// calculate perfect aspect ratio:
		    				// http://ms-studio.net/notes/mathematical-problem/
		    				// divide height by width, then multiply by 100
		    				
		    				$vimeo_aspect = ( $vimeo_height / $vimeo_width);
		    				$vimeo_aspect = ( $vimeo_aspect * 100 );
		    			
		    				?>
		    				<div class="vimeo-item">
		    				
		    					<div class="vimeo-container" style="background-image: url('<?php echo $vimeo_img_large ?>');padding-bottom: <?php echo $vimeo_aspect ?>%;">
		    				<?php
		    				
		    				
		    				
		    				$player_url = 'http://player.vimeo.com/video/' . $video->id . '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1';	
		    				// " frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen			
		    							
		    				?><a href="<?php echo $player_url; ?>" data-vimeo="<?php echo $video->id; ?>" target="_blank" class="jstrigger-vimeo vimeo-placeholder vimeo-img-link vimeoframe unstyled" title="<?php echo esc_attr( $vimeo_title ); ?>" data-caption="<?php 
		    				
		    				echo esc_attr( $vimeo_title );
		    				
		    				?>">
		    				</a>
		    				</div><!-- .vimeo-container -->
		    				
		    					<div class="vid-legende nojust">
		    					  <p class="vid-title" ><?php 
		    						
		    							echo $vimeo_title ; // full title
		    							    					
		    					?></p> 
		    					  <p class="vid-duration"><?php 
		    					
		    					// duration
		    					$videosecs = $video->duration ;
		    					
		    					$init = $videosecs;
		    					$hours = floor($init / 3600);
		    					$minutes = floor(($init / 60) % 60);
		    					$seconds = $init % 60;
		    					
		    					// echo "$hours:$minutes:$seconds";
		    					
		    					printf("%02d:%02d", $minutes, $seconds);
		    					
		    					?> min.</p>
		    					<?php 
		    					
		    					if ($vimeo_type == 'item') {
		    						// full video description ?
		    						 echo '<p class="vid-description small-text">';
		    						 echo $video->description ;
//		    						 echo 'format: '.$vimeo_width.'x'.$vimeo_height.' = ';
		    						 echo '</p>';
		    					}
		    					
		    					// CLOSE div.img-legende
		    					 ?></div><!-- div.img-legende -->
		    				<?php 
		    				
		    				// CLOSING TAGS
		    				// *************
		    				
		    				?>  </div><!-- .vimeo-item -->
		    				
		    				<?php 
		    				
		    		} // end Vimeo Foreach 
		    		
		    				?>
		    			
		      </div><!-- .gallery-inside -->
		    </section> <!--.vimeo-gallery -->
		    <?php
		    
		 } // end test !empty
		 			
?>