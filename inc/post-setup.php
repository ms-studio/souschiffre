<?php
/**
 * Performing some tests upon page load
 * @package souschiffre
 */
 
 // define some variables
 
 $current_post_id = get_the_ID();
 $_ef = '<span class="nbsp">&nbsp;</span>';
 $is_main_page = false;
 $link_to_main_page = false;
 $is_upcoming_event = false;
 
 // test for dates
 
 include( TEMPLATEPATH . '/inc/event-date.php' );
 
 // first thing to do: test if we are PAGE or SUBPAGE. 
  
 // 1: are we Page Principale? Y/N
 			
	if ( has_term( 'projet-principal', 'reglages' ) ) {
			$is_main_page = true;
	} else {
			$is_main_page = false;
	}
 
 // 2: do we have linked pages? Y/N
	$has_linked_pages = false;
	
	// Pages en relation
	
	// run only if we are a POST!
	
	if ( 'post' == get_post_type() ) {
	
			$connected_stuff = new WP_Query( array(
					'posts_per_page' => -1,
					'connected_type' => 'projects',
					'nopaging' => true,
					'connected_items' => $current_post_id, 
					'orderby' => 'title',
					'order' => 'ASC', // desc = newest first
			) );
			
			// list connected stuff
			// and make an array
			$content_sub_array = array();
			
			if ( $connected_stuff->have_posts() ) :
							
						$has_linked_pages = true;
			      
				     while ( $connected_stuff->have_posts() ) : $connected_stuff->the_post(); 
				      	// add to $content_sub_array...
				      	
				      	// test if item is Main Content
				      	if ( has_term( 'projet-principal', 'reglages' ) ) {
				      			$item_status = "main";
				      			$item_title = get_the_title();
				      			$link_to_main_page = true;
				      	} else {
				      			$item_status = "normal";
				      			$item_title =  souschiffre_title_wrangler();
				      	}
				      	
				      	$content_sub_array[] = array(
				      			"id" => get_the_ID(),
				      			"title" => $item_title,
				      			"url" => get_permalink(),
				      			"status" => $item_status,
				      			"content" => get_the_content_with_formatting(),
				      	);
				      	
				     endwhile;
				     
							wp_reset_postdata();
			else :
			endif;
			// end stuff
	}

// now we can define the post status:
$this_post_status = 'undefined';


	if ( $is_main_page == true ) {
		
		// 1) a main page (maybe with sub pages)
		$this_post_status = 'master';
	
	} else { // not main page
		
		if ( $link_to_main_page == true) {
		
			// 2) a sub-page with a main page
			$this_post_status = 'sub-with-master';
			
			// detect the master item:
			function find_master_page($item){
					return (is_array($item) && $item['status'] == 'main');
				}
			$filtered_sub_array = array_filter($content_sub_array,"find_master_page");
			
		} else {
		
			// 3) a sub-page without a main page
			$this_post_status = 'sub-without-master';
		}
	
	}
	
// produce a clean title:
// http://stackoverflow.com/questions/3363005/how-to-remove-numbers-from-a-string-with-regex

$cleantitle = get_the_title();
$cleantitle = ltrim($cleantitle, ' 0123456789');

// end of testing
