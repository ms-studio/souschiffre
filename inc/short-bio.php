<?php 

?>

<div class="shift-left short-bio-container">

<aside id="author-bio" class="author-bio">
      <div class="author-blurb author clear">
      
          <div class="vcard">
	          <p><span class="fn"><a href="http://souschiffre.net/a-propos-de/" title="Dorothée Thébert-Filliger" rel="author" class="unstyled">Dorothée Thébert-Filliger</a></span> est photographe et metteuse en scène.</p>
	          <p class="more-info"><a href="http://souschiffre.net/a-propos-de/" class="unstyled hoverable">En savoir plus ...</a></p>
	          
	          <div class="social-networks">
	          	<?php wp_nav_menu( array( 'theme_location' => 'footerlinks' ) ); ?>
	          </div>
	          
	          
          </div>
          
          <a href="http://souschiffre.net/a-propos-de/"><img alt='' src='<?php 
          
          echo $GLOBALS["TEMPLATE_RELATIVE_URL"];
          
           ?>img/portraits/portrait-01.jpg' class='avatar avatar-128 photo' /></a>
       
       </div>
       
</aside>
<!-- end #author-bio -->

<?php 

		// test for upcoming content...

if ( false === ( $nfo_footer_events = get_transient( 'nfo_footer_events' ) ) ) {
		
		$nfo_unix_year = (  1 * 14 * 24 * 60 * 60 ); // limit: how old can it be ?
			//             months * days * hours
			$nfo_unix_limit = ( $nfo_unix_now - $nfo_unix_year );
			$nfo_age_limit = date_i18n( "Y-m-d", $nfo_unix_limit);
			$exclude_id = array();
			
			$nfo_footer_events = new WP_Query( array( 
					'posts_per_page' => -1,
					
					'meta_key' => '_mem_start_date',
					'meta_value' => $nfo_age_limit,
					'meta_compare' => '>', 
					// = newer than Age Limit
					
					'orderby'  => 'meta_value',
					'order'  => 'ASC', // ASC: oldest first
			 ) );
			 
			set_transient( 'nfo_footer_events', $nfo_footer_events, 60*60*3 ); // 3 heures
			
} // end of get_transient test
					
		if ($nfo_footer_events->have_posts()) : 
				?>
				<section class="footer-upcoming">
					<h1 class="widget-title">Actualités</h1>
				<?php
				
				while( $nfo_footer_events->have_posts() ) : $nfo_footer_events->the_post();  
				
				?><article>
					<a class="unstyled" href="<?php the_permalink() ?>">
						<h2 class="hoverable"><?php 
					
					the_title();
					
					// since we're at it, let's also add the ID to the exclude list
					$exclude_id[] = get_the_ID();
					
					 ?></h2>
					 
					 <?php 
					 
					 include( TEMPLATEPATH . '/inc/event-date.php' );
					 
					 if ($event_date != '') { 
					 
					 	echo '<p class="simple"><time itemprop="startDate" datetime="'. esc_attr($start_date_iso) .'">';
					 	echo $event_date;
					 	echo '</time></p>';
					 }
					 
					 // test for location:
					 /*
					  * TEST LIEUX
					 	*/
					 						
					 			$leslieux = get_the_terms($post->ID, 'lieux' );
					 
					 			if ($leslieux) {
					 			
					 				$lieux_array = array();
					 				
					 				// echo count($series_array);
					 				
					 				foreach($leslieux as $item) {
					 						// add item to array...
					 						$lieux_array[] = array( 
					 								"name" => $item->name, 
					 						    	"url" => get_term_link($item->slug, 'lieux'),
					 						    	"count" => $item->count,
					 						    	"id" => $item->term_id,
					 						   );
					 					}
					 					
					 					?>
					 						  	<p class="lieu simple"><?php
					 						   
					 					  		// output everything...
					 					  		foreach ($lieux_array as $key => $row){
					 					  			echo $lieux_array[$key]["name"] ; 	   		
					 					  		}
					 					  		
					 					  		// how many lieux do we have ?
					 					  		// echo count($lieux_array);
					 					  		// count items of serie #1
					 					  		$series_item_count = $lieux_array[0]["count"];
					 					  	
					 					  ?></p>
					 					  <?php
					 				
					 			} // end testing for LIEUX.
					 
					  ?>
					  </a>
				</article>
				<?php
				
				endwhile; 
				
				?></section>
				<?php
				
				endif;
				wp_reset_postdata(); 
		
?>

</div>

<?php 


 ?>