<?php
/**
 * @package souschiffre
 */
 
/**
 * 
 * This page puts the content into an array... 
 */
 
 // Initialize:
 $item_status = "neutral";
 $archive_sub_array = array();
 $current_post_id = get_the_ID();
 
 // Evaluate situation: 
 // 1) IS THIS a main item, or sub-item ?
 
 if ( has_term( 'projet-principal', 'reglages' ) ) {
 		
 		$item_status = "main";
 		
 		 // 2) Are there any attached posts?
 		// - Posts-2-Posts ?? - Are there attached posts ?
 		
 		// if attached posts: 
 		// add them ALL to sub-array
 		
 				
 				$connected_stuff = new WP_Query( array(
 						'posts_per_page' => -1,
 						'connected_type' => 'projects',
 						'nopaging' => true,
 						'connected_items' => $current_post_id, // get_queried_object_id(), // $this_post_id
 						'orderby' => 'title',
 						'order' => 'ASC', // desc = newest first
 				) );
 				
 				// list connected stuff
 				if ( $connected_stuff->have_posts() ) :
 				      
 					     while ( $connected_stuff->have_posts() ) : $connected_stuff->the_post(); 
 					     
 					      	// add to $archive_sub_array...
 					      	
 					      	$regex_name =  souschiffre_title_wrangler();
 					      	
 					      	$archive_sub_array[] = array(
 					      	 				"title" => $regex_name, // get_the_title(),
 					      	 				"id" => $current_post_id,
 					      	 				"url" => get_permalink(),
 					      	 );
 					      	
 					      	$exclude_id[] = get_the_ID();
 					      
 					     endwhile;
 								// Prevent weirdness
 								wp_reset_postdata();
 							
 				else :
 							
 				endif;
 				// end stuff
 		
 } else { // is a sub-item
 
 		// if has attached MAIN post: DO nothing
 		
 		
 		// WARNING: test if we have a *post*, or some other content type 
 		// otherwise we get errors for Pages.
 		
 		$connected_stuff = new WP_Query( array(
 				'posts_per_page' => -1,
 				'connected_type' => 'projects',
 				'nopaging' => true,
 				'connected_items' => $current_post_id,
 				'tax_query' => array(
 				  		array(
 				  			'taxonomy' => 'reglages',
 				  			'field' => 'slug', // ('id' or 'slug')
 				  			'terms' => 'projet-principal',
 				  			// 'operator' => 'NOT IN'
 				  		)
 				  	),
 				'orderby' => 'date',
 				'order' => 'DESC', // desc = newest first
 		) );
 		
 		if ( $connected_stuff->have_posts() ) :
 		
 			$item_status = "sub-item";
// 			break 2; 

		else : 
		
		// if NO MAIN POST: 
		// test for linked articles, by date:
		// the oldest will be considered the MAIN ONE.
 		
 		endif;
 		 		
 }
 
 
 include( TEMPLATEPATH . '/inc/event-date.php' );
 
 if ($event_date !== '') { 
	 	$item_date = $start_date;
	 	// $event_date is set.
	 	
	 	if ( $event_is_future == true ) {
	 			
	 			// we show the full $event_date;
	 			
	 			} else {
	 		
	 			// past event!
	 			$event_date = $event_date_yr;
	 		
	 		}
	 	
 } else {
	 
	 	// we have no event date - let's use the publication date.
	 	$item_date = get_the_date('Y-m-d h:i:s'); 
//	 	$event_date = get_the_date();
 		$event_date = '';
 }		
 
 // test for the Featured Image
 
 	$featured_image_url = '';
 	include( TEMPLATEPATH . '/inc/featured-img-loop.php' );
 	
 	// test if "extrait_format_large" ACF field is defined.
 	
 	$img_format_large_id = get_post_meta($post->ID, 'extrait_format_large', true);
 	
 	if ($img_format_large_id) {
 		// retrieve FULL image url
 		$full_image_url = wp_get_attachment_image_src( $img_format_large_id, 'full');
 		$featured_image_url = $full_image_url[0];
 	}
 
 $archive_array[] = array(
 				"title" => get_the_title(),
 				"id" => $current_post_id,
 				"url" => get_permalink(),
 				"status" =>  $item_status,
 				"sub-array" => $archive_sub_array,
 				
// 				"img-large" => $video->thumbnail_large,
// 				"img-medium" => $video->thumbnail_medium,
				"image-url" => $featured_image_url,
 				"pubdate" => $item_date, // format: 2012-09-12 06:37:21 
 				"pubdate-human" => $event_date, // format: 2012-09-12 06:37:21 
 );

// ajouter l'ID au "exclude"
$exclude_id[] = get_the_ID();

?>