<?php
/**
 * @package souschiffre
 */
 
 if ($item['status'] == "sub-item" ) {
 
 	// do noting
 
 } else {
 
?>

<article id="post-<?php echo $item['id']; ?>" class="archive-<?php echo $item['status']; ?>">
	<header class="entry-header">
		<h1 class="entry-title"><a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></h1>

		<div class="entry-meta">
			<?php 
			echo $item['pubdate-human'];
		?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	
	<?php 
	
	if ($item['sub-array'] !== '') {
			
			foreach ($item['sub-array'] as $key => $item) { 
			
				?>
				<h2 class="entry-title"><a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></h2>
				<?php
			}
	
	}
	
	 ?>

</article><!-- #post-## -->

<?php

}

?>