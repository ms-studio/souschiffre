<?php
/**
 * souschiffre Featured Image Query
 *
 * @package souschiffre
 */

// test if there is a large picture

// 1) test if there's a featured image

if ( '' != get_the_post_thumbnail() ) {
    
    // YES, featured image is defined.
    // 2) is it big enough?
    
       $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'widescreen');
       
    	// produces an array. get the img url:
    	
    	$featured_image_url = $large_image_url[0];

    	$has_image = true;
    
} else {
        
    // 3) test the first attached image?
    	      			
    $img_info = img_toolbox_array( 'widescreen', 1); 
    
    $url_widescreen = $img_info[0]["url-custom"];
    
    // if yes, display it and break
    
    if (!empty($url_widescreen)) {
    
//    			echo '<h2>';
//    			 the_title();
//    			 echo '</h2>'; 
    			     			 
    			 $featured_image_url = $url_widescreen;
    			 $has_image = true;
    }
}

// else, continue looping...

?>