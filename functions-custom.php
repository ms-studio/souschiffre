<?php
/**
 * souschiffre functions and definitions
 *
 * @package souschiffre
 */
 
 
 // Custom Functions for CSS/Javascript Versioning
 $GLOBALS["TEMPLATE_URL"] = get_bloginfo('template_url')."/";
 $GLOBALS["TEMPLATE_RELATIVE_URL"] = wp_make_link_relative($GLOBALS["TEMPLATE_URL"]);
 
 
 /** plugin options **/
 function my_mem_settings() {
 	mem_plugin_settings( array( 'post' ), 'full' );
 }
 add_action( 'mem_init', 'my_mem_settings' );
 
 
 
 add_post_type_support( 'page', 'excerpt' );
 
 
 
 /*
 Disable post formats
 courtesy Justin Tadlock
 https://github.com/justintadlock/disable-post-format-ui/blob/master/disable-post-format-ui.php
 */
 add_filter( 'enable_post_format_ui', '__return_false' );
 
 
 
 // Custom image sizes
 // ****************************** 
  
  if ( function_exists( 'add_image_size' ) ) { 
  	//add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
  	// thumbnail: 150, 150, true
  	add_image_size( 'standard', 492, 984 );
  	// gutter = 20px
  	// 492-20 = 472, 472/2=236
  	add_image_size( 'horiz-thumb', 236, 177, true ); // format 4/3
  	
  	add_image_size( 'widescreen', 1024, 384, true );
//  	add_image_size( 'vertical-thumb', 177, 236, true );
  	// medium: 300, 300
  	// large: 1024, 1024
//  	add_image_size( 'superwide', 1280, 196, true );
  	//add_image_size( 'landscape', 304, 184, true ); // true = cropped
  }
 
 /*
  * custom taxonomies
  ********************
  * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
 
 
 //create two taxonomies, genres and writers for the post type "book"
 function souschiffre_taxonomies() 
 {
   register_taxonomy('lieux','post',array( 
    		'hierarchical' => false, 
    		'label' => 'Lieux',
    		'labels'  => array(
    			'name'                => _x( 'Lieux', 'taxonomy general name' ),
    			'singular_name'       => _x( 'Lieu', 'taxonomy singular name' ),
    			'search_items'        => __( 'Chercher dans les lieux' ),
    			'menu_name'           => __( 'Lieux' )
    		),
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array('slug' => 'lieux'),
    		'singular_label' => 'Lieu') 
    );
   	 
   register_taxonomy('reglages','post',array( 
   	 		'hierarchical' => true, 
   	 		'label' => 'Réglages',
   	 		'labels'  => array(
   	 			'name'                => _x( 'Réglages', 'taxonomy general name' ),
   	 			'singular_name'       => _x( 'Réglage', 'taxonomy singular name' ),
   	 			'search_items'        => __( 'Chercher dans les réglages' ),
   	 			'menu_name'           => __( 'Réglages' )
   	 		),
   	 		'show_ui' => true,
   	 		'query_var' => true,
   	 		'rewrite' => array('slug' => 'reglages'),
   	 		'singular_label' => 'Réglage') 
   	 );
   
 } // end souschiffre_taxonomies()  function
  //hook into the init action and call souschiffre_taxonomies() when it fires
  add_action( 'init', 'souschiffre_taxonomies', 0 );
 
/*
 * Posts 2 Posts
 ***************
*/
function souschiffre_p2p() {

// Make sure the Posts 2 Posts plugin is active.
if ( !function_exists( 'p2p_register_connection_type' ) )
	return;
	
p2p_register_connection_type( array( 
			'name' => 'projects',
			'from' => 'post',
			'to' => 'post',
			'reciprocal' => true,
			'title' => 'Connexion',
			// 'sortable' => 'any' 
//			'fields' => array(
//					'count' => array(
//						'title' => 'Ordre',
//						'type' => 'text',
//					),
//			),
		) );
}
add_action( 'p2p_init', 'souschiffre_p2p' );

// info
// https://github.com/scribu/wp-posts-to-posts/wiki/Connection-ordering
	 
/*
 * Image Array
 *****************
*/
 
 
function img_toolbox_array($size = 'thumbnail', $howmany = -1) {
	if ( $images = get_children ( array (
		'post_parent'    => get_the_ID(),
		'post_type'      => 'attachment',
		'numberposts'    => $howmany , // -1 = show all
		'post_status'    => null,
		'post_mime_type' => 'image',
		'orderby' => 'menu_order',
		'order' => 'ASC',
	))) {
		$img_gallery_array = array();
		$img_gallery = array();
		foreach ( $images as $image ) {
			
			$img_id = $image->ID;
			$img_custom_size = image_downsize( $img_id, $size );
			$img_large_size = wp_get_attachment_image_src( $img_id, 'large' );
			$img_full_size = wp_get_attachment_image_src( $img_id, 'full' );
			$img_horiz_thumb = wp_get_attachment_image_src( $img_id, 'horiz-thumb' );
			
			$img_gallery_array[] = array( 
					"id" => $img_id,
						// URLs 
			    	"url-custom" => $img_custom_size[0],
			    	"url-large" => $img_large_size[0],
			    	"url-full" => $img_full_size[0],
			    	"url-horiz-thumb" => $img_horiz_thumb[0],
			    	// dimensions
			    	"width-custom" => $img_custom_size[1],
			    	"height-custom" => $img_custom_size[2],
			    	// defaults
			    	"width-large" => $img_large_size[1],
			    	"height-large" => $img_large_size[2],
			    	"width-full" => $img_full_size[1],
			    	"height-full" => $img_full_size[2],
			    	// text
			    	"title" => apply_filters('the_title',$image->post_title),
// error in 3.6-alpha
// https://gist.github.com/ms-studio/3928765#comment-824110
			    	"caption" => $image->post_excerpt,
			    	"description" => $image->post_content,
			    	"alt-text" => get_post_meta($img_id, '_wp_attachment_image_alt', true),
			   );
		} // end foreach
		
		$img_gallery[0]["images"] = $img_gallery_array;
		return $img_gallery;
	} 
}

function img_toolbox_processor() {
}

function gallery_toolbox() {
	
	$img_gallery = array();
		
	while( has_sub_field('bloc_galeries_photos') ): 
	
			$gallery_title = get_sub_field('titre_galerie');
			//$img_gallery[]["title"] = $gallery_title;
			
			$gallery_description = get_sub_field('descriptif_galerie');
			//$img_gallery[]["description"] = $gallery_description;
			
			$images = get_sub_field('galerie_photo');
			$img_array = array();
			foreach ( $images as $image ) {
					
					$img_array[] = array( 
							"id" => $image["id"],
							"url-custom" => $image["sizes"]["thumbnail"],
							"url-large" => $image["sizes"]["large"],
							"width-custom" => $image["sizes"]["thumbnail-width"],
							"height-custom" => $image["sizes"]["thumbnail-height"],
							"caption" => $image["caption"],
							"gallery-title" => $gallery_title,
							"gallery-descr" => $gallery_description,
					);
					
			} // end foreach
			
			//$img_gallery[]["images"] = $img_array;
			
			$img_gallery[] = array(
				"title" => $gallery_title,
				"description" => $gallery_description,
				"images" => $img_array,
			);
	 							
	endwhile; 
	
//		var_dump($img_gallery);
	 return $img_gallery;
}

/*
 * Global Date Variables
*/
// The current year variable
// ******************************
// loaded in the header.php

function nfo_date_of_today() {
    
   global $nfo_aujourdhui_now;
   $nfo_aujourdhui_now = date_i18n( "j F Y - H:i:s"); 
       
   global $nfo_aujourdhui;
   $nfo_aujourdhui = date_i18n( "j F Y");
   
   global $mem_today_short;
   $mem_today_short = date_i18n( "Y-m-d");
   
   global $nfo_isoweek;
   $nfo_isoweek = date_i18n( "W");
   
   global $nfo_unix_now;
   $nfo_unix_now = strtotime( $mem_today_short );
   
   // current year info
   
   global $nfo_current_year;
   $nfo_current_year = date_i18n( "Y");
   
   global $nfo_current_year_string;
   $nfo_current_year_string = $nfo_current_year . "-01-01";
   
   global $nfo_current_year_end;
   $nfo_current_year_end = $nfo_current_year . "-12-31 23:59";
   
   // current categories
   
   global $nfo_visible_categories;
   $nfo_visible_categories = 'expo,conference,evenement,workshop,symposium,voyage';
   // used in: home.php, date.php
   // remove: seminaire
   
}

// http://www.web-templates.nu/2008/08/31/get_the_content-with-formatting/index.html

function get_the_content_with_formatting ($more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
	$content = get_the_content($more_link_text, $stripteaser, $more_file);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}


add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {

		// add your extension to the array
		$existing_mimes['svg'] = 'image/svg+xml';

		// removing existing file types
		unset( $existing_mimes['bmp'] );
		unset( $existing_mimes['tif|tiff'] );

		// and return the new full result
		return $existing_mimes;
}
 

 
// vimeo testing function
// source: http://stackoverflow.com/questions/11304044/determining-the-vimeo-source-by-url-regex
 
function discoverVimeo($url) {
    if ((($url = parse_url($url)) !== false)) {
        $url = array_filter(explode('/', $url['path']), 'strlen');
        if (in_array($url[1], array('album', 'channels', 'groups')) !== true) {
            array_unshift($url, 'item');
        } 
        return array('type' => rtrim(array_shift($url), 's'), 'id' => array_shift($url));
    } 
    return false;
}


/* Vimeo API
// https://github.com/vimeo/vimeo-api-examples/blob/master/simple-api/simple/simple.php
******************************/

//require_once('../../../proxy.php');

// Curl helper function
function curl_get($url) {
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
	// include optional proxy information
	// four relative steps = WP base directory
	$return = curl_exec($curl);
	curl_close($curl);
	return $return;
}

 
 
 /**
 * Plugin URI: http://tomjn.com/164/clients-who-upload-huge-camera-photos-decompression-bombs/
 * Version: 1.1
 */
 
// function tomjn_deny_giant_images($file){
// 	$type = explode('/',$file['type']);
//  
// 	if($type[0] == 'image'){
// 		list( $width, $height, $imagetype, $hwstring, $mime, $rgb_r_cmyk, $bit ) = getimagesize( $file['tmp_name'] );
// 			if($width * $height > 3200728){ // I added 100,000 as sometimes there are more rows/columns than visible pixels depending on the format
// 			$file['error'] = 'Cette image est trop grande. Veuillez réduire sa résolution à 2048 sur 1536 pixels.';
// 		}
// 	}
// 	return $file;
// }
// add_filter('wp_handle_upload_prefilter','tomjn_deny_giant_images');
 
// produces error on SousChiffre:
//  
 
 /**
 * end of file
 */
 
 