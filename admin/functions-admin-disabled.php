<?php  


// Add custom taxonomies and custom post types counts to dashboard
// 
// http://codex.wordpress.org/Plugin_API/Action_Reference/right_now_content_table_end

add_action( 'right_now_content_table_end', 'my_add_counts_to_dashboard' );

function my_add_counts_to_dashboard() {

    // Custom post types counts
    $post_types = get_post_types( array( '_builtin' => false ), 'objects' );
    foreach ( $post_types as $post_type ) {
        $num_posts = wp_count_posts( $post_type->name );
        $num = number_format_i18n( $num_posts->publish );
        $text = _n( $post_type->labels->singular_name, $post_type->labels->name, $num_posts->publish );
        if ( current_user_can( 'edit_posts' ) ) {
            $num = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . '</a>';
            $text = '<a href="edit.php?post_type=' . $post_type->name . '">' . $text . '</a>';
        }
        echo '<td class="first b b-' . $post_type->name . 's">' . $num . '</td>';
        echo '<td class="t ' . $post_type->name . 's">' . $text . '</td>';
        echo '</tr>';

        if ( $num_posts->pending > 0 ) {
            $num = number_format_i18n( $num_posts->pending );
            $text = _n( $post_type->labels->singular_name . ' pending', $post_type->labels->name . ' pending', $num_posts->pending );
            if ( current_user_can( 'edit_posts' ) ) {
                $num = '<a href="edit.php?post_status=pending&post_type=' . $post_type->name . '">' . $num . '</a>';
                $text = '<a href="edit.php?post_status=pending&post_type=' . $post_type->name . '">' . $text . '</a>';
            }
            echo '<td class="first b b-' . $post_type->name . 's">' . $num . '</td>';
            echo '<td class="t ' . $post_type->name . 's">' . $text . '</td>';
            echo '</tr>';
        }
    }
}




// Admin Interface improvement

function souschiffre_admin_head() {
        echo '<link rel="stylesheet" type="text/css" href="' .get_bloginfo('template_directory').'/admin/admin.css">';
}
add_action('admin_head', 'souschiffre_admin_head');


/*
adding columns for Materialien
sources: 

http://codex.wordpress.org/Plugin_API/Filter_Reference/manage_edit-post_type_columns
and
http://yoast.com/custom-post-type-snippets/
*/

// Change the columns for the edit CPT screen

function change_columns( $cols ) {
  $cols = array(
    'cb'       => '<input type="checkbox" />',
    'title' => __('Title'),
    'groupes' => __('Groupe'),
    'date' => __('Date'),
    //'url'      => __( 'URL',      'trans' ),
  );
  return $cols;
}
add_filter( "manage_personnes_posts_columns", "change_columns" );

// Give these new columns some content
function custom_columns( $column, $post_id ) {
  switch ( $column ) {
    case "url":
      $url = get_post_meta( $post_id, 'url', true);
      echo '<a href="' . $url . '">' . $url. '</a>';
      break;
    case 'groupes':
    			$terms = get_the_term_list( $post->ID , 'groupes' , '' , ', ' , '' );
    			if ( is_string( $terms ) ) {
    				echo $terms;
    			}  
    			else 
    			{
    				echo 'nada';
    			}
    			
    			break;


    case "host":
      echo get_post_meta( $post_id, 'host', true);
      break;
  }
}

add_action( "manage_posts_custom_column", "custom_columns", 10, 2 );

// Make these columns sortable
function sortable_columns() {
  return array(
    //'kk_kategorie'      => 'kk_kategorie',
    // not working on taxonomies
    'title' => 'title',
    'date'     => 'date'
  );
}

add_filter( "manage_edit-groupes_sortable_columns", "sortable_columns" );


// add list menus for custom post types:
// source:  http://yoast.com/custom-post-type-snippets/

// Filter the request to just give posts for the given taxonomy, if applicable.
//function taxonomy_filter_restrict_manage_posts() {
//    global $typenow;
//
//    $post_types = get_post_types( array( '_builtin' => false ) );
//
//    if ( in_array( $typenow, $post_types ) ) {
//    	$filters = get_object_taxonomies( $typenow );
//
//        foreach ( $filters as $tax_slug ) {
//            $tax_obj = get_taxonomy( $tax_slug );
//            wp_dropdown_categories( array(
//                'show_option_all' => __('Tous les '.$tax_obj->label ),
//                'taxonomy' 	  => $tax_slug,
//                'name' 		  => $tax_obj->name,
//                'orderby' 	  => 'name',
//                'selected' 	  => $_GET[$tax_slug],
//                'hierarchical' 	  => $tax_obj->hierarchical,
//                'show_count' 	  => false,
//                'hide_empty' 	  => true
//            ) );
//        }
//    }
//}
//
//add_action( 'restrict_manage_posts', 'taxonomy_filter_restrict_manage_posts' );

// And then, we add a filter to the query so the dropdown will actually work:

//function taxonomy_filter_post_type_request( $query ) {
//  global $pagenow, $typenow;
//
//  if ( 'edit.php' == $pagenow ) {
//    $filters = get_object_taxonomies( $typenow );
//    foreach ( $filters as $tax_slug ) {
//      $var = &$query->query_vars[$tax_slug];
//      if ( isset( $var ) ) {
//        $term = get_term_by( 'id', $var, $tax_slug );
//        $var = $term->slug;
//      }
//    }
//  }
//}
//
//add_filter( 'parse_query', 'taxonomy_filter_post_type_request' );

// triggers an error:
//  Notice: Trying to get property of non-object in /wp-content/themes/souschiffre/admin/functions-admin.php on line 161


/*
Plugin URI: http://www.speckygeek.com
Description: Add custom styles in your posts and pages content using TinyMCE WYSIWYG editor. The plugin adds a Styles dropdown menu in the visual post editor.
Based on TinyMCE Kit plug-in for WordPress
http://plugins.svn.wordpress.org/tinymce-advanced/branches/tinymce-kit/tinymce-kit.php
*/
/**
 * Apply styles to the visual editor
 */
function tuts_mcekit_editor_style($url) {
    if ( !empty($url) )
        $url .= ',';
    // Retrieves the plugin directory URL
    // Change the path here if using different directories
    $url .= get_bloginfo('template_directory') . '/admin/editor-styles.css';
    return $url;
}
//add_filter('mce_css', 'tuts_mcekit_editor_style');
/**
 * Add "Styles" drop-down
 */
function tuts_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
//add_filter( 'mce_buttons_2', 'tuts_mce_editor_buttons' );
/**
 * Add styles/classes to the "Styles" drop-down
 */
function tuts_mce_before_init( $settings ) {
    $style_formats = array(
				array(
				    'title' => 'Sans retrait',
				    'block' => 'div',
				    'classes' => 'sans-retrait',
				    'wrapper' => true
				),
        array(
            'title' => 'Boîte infos',
            'block' => 'div',
            'classes' => 'boite-infos',
            'wrapper' => true
        ),
        array(
            'title' => 'Citation',
            'block' => 'blockquote',
            'classes' => 'blockquote',
            'wrapper' => true
        ),
    );
    $settings['style_formats'] = json_encode( $style_formats );
    $settings['theme_advanced_disable'] = 'justifyleft, justifycenter, justifyright, justifyfull, fontselect, fontsizeselect, forecolor, backcolor, forecolorpicker, backcolorpicker, formatselect';
    return $settings;
}
//add_filter( 'tiny_mce_before_init', 'tuts_mce_before_init' );

/* Learn TinyMCE style format options at http://www.tinymce.com/wiki.php/Configuration:formats */
/*
 * Add custom stylesheet to the website front-end with hook 'wp_enqueue_scripts'
 */

/*
 * Enqueue stylesheet, if it exists.
 */
 
function tuts_mcekit_editor_enqueue() {
  $StyleUrl = get_bloginfo('template_directory') .'/admin/editor-styles.css';
  wp_enqueue_style( 'myCustomStyles', $StyleUrl );
}
//add_action('wp_enqueue_scripts', 'tuts_mcekit_editor_enqueue');

// INFO SOURCES:
// http://codex.wordpress.org/TinyMCE
// http://www.wdmac.com/how-to-use-custom-styles-in-the-word-press-post-editor

?>