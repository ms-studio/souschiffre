<?php  


// Add custom taxonomies and custom post types counts to dashboard
// 
// http://codex.wordpress.org/Plugin_API/Action_Reference/right_now_content_table_end

add_action( 'right_now_content_table_end', 'my_add_counts_to_dashboard' );

function my_add_counts_to_dashboard() {

    // Custom post types counts
    $post_types = get_post_types( array(
    	'_builtin' => false,
    	'public'   => true 
    	), 'objects' );
    foreach ( $post_types as $post_type ) {
        $num_posts = wp_count_posts( $post_type->name );
        $num = number_format_i18n( $num_posts->publish );
        $text = _n( $post_type->labels->singular_name, $post_type->labels->name, $num_posts->publish );
        if ( current_user_can( 'edit_posts' ) ) {
            $num = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . '</a>';
            $text = '<a href="edit.php?post_type=' . $post_type->name . '">' . $text . '</a>';
        }
        echo '<td class="first b b-' . $post_type->name . 's">' . $num . '</td>';
        echo '<td class="t ' . $post_type->name . 's">' . $text . '</td>';
        echo '</tr>';

        if ( $num_posts->pending > 0 ) {
            $num = number_format_i18n( $num_posts->pending );
            $text = _n( $post_type->labels->singular_name . ' pending', $post_type->labels->name . ' pending', $num_posts->pending );
            if ( current_user_can( 'edit_posts' ) ) {
                $num = '<a href="edit.php?post_status=pending&post_type=' . $post_type->name . '">' . $num . '</a>';
                $text = '<a href="edit.php?post_status=pending&post_type=' . $post_type->name . '">' . $text . '</a>';
            }
            echo '<td class="first b b-' . $post_type->name . 's">' . $num . '</td>';
            echo '<td class="t ' . $post_type->name . 's">' . $text . '</td>';
            echo '</tr>';
        }
    }
}

// Admin Interface improvement

function souschiffre_admin_head() {
        echo '<link rel="stylesheet" type="text/css" href="' .get_bloginfo('template_directory').'/admin/admin.css">';
}
add_action('admin_head', 'souschiffre_admin_head');


function souschiffre_remove_dashboard_widgets()
{
	// Globalize the metaboxes array, this holds all the widgets for wp-admin
	global $wp_meta_boxes;
	
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	
}
add_action('wp_dashboard_setup', 'souschiffre_remove_dashboard_widgets' );


/**
 * Show recent posts : 
 * https://gist.github.com/ms-studio/6069116
 */

function wps_recent_posts_dw() {
?>
   <ol>
     <?php
          global $post;
          $args = array( 'numberposts' => 5 );
          $myposts = get_posts( $args );
                foreach( $myposts as $post ) :  setup_postdata($post); ?>
                    <li> <? the_time('j F Y'); ?> – <a href="<?php echo admin_url(); ?>post.php?post=<?php the_ID(); ?>&action=edit"><?php the_title(); ?></a> (<a href="<?php the_permalink(); ?>">visiter</a>)</li>
          <?php endforeach; ?>
   </ol>
<?php
}
function add_wps_recent_posts_dw() {
       wp_add_dashboard_widget( 'wps_recent_posts_dw', __( 'Recent Posts' ), 'wps_recent_posts_dw' );
}
add_action('wp_dashboard_setup', 'add_wps_recent_posts_dw' );



/* edit screen improvements
************************/

function remove_edit_fields() {
	
	/* Slug meta box. */
//	remove_meta_box( 'slugdiv', 'post', 'normal' );
	// remove comments status
	remove_meta_box( 'commentstatusdiv' , 'post' , 'normal' ); 
	// remove comments
	remove_meta_box( 'commentsdiv' , 'post' , 'normal' ); 
	// remove author 
	remove_meta_box( 'authordiv' , 'post' , 'normal' ); 
	/* Post format meta box. */
	remove_meta_box( 'formatdiv', 'post', 'normal' );
	/* Trackbacks meta box. */
	remove_meta_box( 'trackbacksdiv', 'post', 'normal' );
	/* Custom fields meta box. */
	remove_meta_box( 'postcustom', 'post', 'normal' );
	
}
add_action( 'add_meta_boxes' , 'remove_edit_fields' );

// src: http://codex.wordpress.org/Function_Reference/remove_meta_box
// http://justintadlock.com/archives/2011/04/13/uncluttering-the-post-editing-screen-in-wordpress



/**
 * Apply styles to the visual editor
 */
function tuts_mcekit_editor_style($url) {
    if ( !empty($url) )
        $url .= ',';
    // Retrieves the plugin directory URL
    // Change the path here if using different directories
    $url .= get_bloginfo('template_directory') . '/admin/editor-styles.css';
    return $url;
}
add_filter('mce_css', 'tuts_mcekit_editor_style');

/**
 * Add "Styles" drop-down
 */
function tuts_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter( 'mce_buttons_2', 'tuts_mce_editor_buttons' );
/**
 * Add styles/classes to the "Styles" drop-down
 */
function tuts_mce_before_init( $settings ) {
    $style_formats = array(
				array(
				    'title' => 'Sans retrait',
				    'block' => 'div',
				    'classes' => 'sans-retrait',
				    'wrapper' => true
				),
        array(
            'title' => 'Boîte infos',
            'block' => 'div',
            'classes' => 'boite-infos',
            'wrapper' => true
        ),
        array(
            'title' => 'Citation',
            'block' => 'blockquote',
            'classes' => 'blockquote',
            'wrapper' => true
        ),
    );
    $settings['style_formats'] = json_encode( $style_formats );
    $settings['theme_advanced_disable'] = 'justifyleft, justifycenter, justifyright, justifyfull, fontselect, fontsizeselect, forecolor, backcolor, forecolorpicker, backcolorpicker, formatselect';
    return $settings;
}
add_filter( 'tiny_mce_before_init', 'tuts_mce_before_init' );

/* Learn TinyMCE style format options at http://www.tinymce.com/wiki.php/Configuration:formats */
/*
 * Add custom stylesheet to the website front-end with hook 'wp_enqueue_scripts'
 */

/*
 * Enqueue stylesheet, if it exists.
 */
 
function tuts_mcekit_editor_enqueue() {
  $StyleUrl = get_bloginfo('template_directory') .'/admin/editor-styles.css';
  wp_enqueue_style( 'myCustomStyles', $StyleUrl );
}
add_action('wp_enqueue_scripts', 'tuts_mcekit_editor_enqueue');

// INFO SOURCES:
// http://codex.wordpress.org/TinyMCE
// http://www.wdmac.com/how-to-use-custom-styles-in-the-word-press-post-editor

?>