<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package souschiffre
 */
 

 nfo_date_of_today();
 // located in functions-custom.php
 // defines key values 
 
 // if we are a POST, do some testing:
 
 if ( is_singular( 'post' ) ) {
   include( TEMPLATEPATH . '/inc/post-setup.php' ); 
 }
 
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
			if(is_front_page()){
				echo "souschiffre – Dorothée Thébert-Filliger";
			}  else if ( is_single() ) {

 // wp_title( '|', true, 'right' );
			echo $cleantitle;
			echo ' – souschiffre';
			} else {
				wp_title('–',true,'right');
				// bloginfo('name');
			}
 ?></title>

<?php 
// ** DESCRIPTION v.0.3 **
// https://gist.github.com/ms-studio/1628192
if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); 
?><meta name="description" content="<?php  
	$descr = get_the_excerpt();
	$text = str_replace( '/\r\n/', ', ', trim($descr) ); 
	echo esc_attr($text);
?>" />
<?php endwhile; endif; elseif(is_home()) : 
?><meta name="description" content="Dorothée Thébert-Filliger, photographe, metteuse en scène." />
<?php endif; ?>

<?php 
// ** SEO Robot Tags ** 
// https://gist.github.com/ms-studio/5327837
if ( is_attachment() ) {
?><meta name="robots" content="noindex,follow" /><?php
} else if( is_single() || is_page() || is_home() ) { 
?><meta name="robots" content="all,index,follow" /><?php 
} else if ( is_category() || is_archive() ) { 
?><meta name="robots" content="noindex,follow" /><?php } 
?>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"]; ?>js/html5.js" type="text/javascript"></script>
<![endif]-->
<link href='http://fonts.googleapis.com/css?family=Cardo:400,400italic,700&subset=latin' rel='stylesheet' type='text/css'>

<style>.hidden {display: none;}</style>

<?php 

wp_head(); 

?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	<header id="masthead" class="site-header" role="banner">
		<div class="header-text">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		</div>

		<nav id="site-navigation" class="navigation-main" role="navigation">
			<h1 class="menu-toggle"><?php _e( 'menu', 'souschiffre' ); ?></h1>
			<div class="screen-reader-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'souschiffre' ); ?>"><?php _e( 'Skip to content', 'souschiffre' ); ?></a></div>

			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="main" class="site-main">
