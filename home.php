<?php
/**
 * The home template file.
 *
 * @package souschiffre
 */

//get_header(); 
include( TEMPLATEPATH . '/header.php' );

?>

<!-- home.php -->

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<?php 
		/*
		First, we create a loop that searched for Upcoming Events
		*/
		
		// global $nfo_current_year_string; // = 1er janvier de l'année actuelle
		// global $mem_today_short; // = YYYY-mm-dd
		// $nfo_unix_now : is declared in functions / header
		
		$nfo_unix_year = (  1 * 14 * 24 * 60 * 60 ); // limit: how old can it be ?
		//             months * days * hours
		$nfo_unix_limit = ( $nfo_unix_now - $nfo_unix_year );
		// transform into string
		$nfo_age_limit = date_i18n( "Y-m-d", $nfo_unix_limit);
		
		$exclude_id = array();
		// create an array to prevent stuff from appearing two times
		
		global $wp_query;
		$args = array_merge( $wp_query->query, array( 
				'posts_per_page' => -1,
				
				'meta_key' => '_mem_start_date',
				'meta_value' => $nfo_age_limit,
				'meta_compare' => '>', 
				// = newer than Age Limit
				
				'orderby'  => 'meta_value',
				'order'  => 'ASC', // ASC: oldest first
		 ) );
		
	query_posts( $args );
	
	if (have_posts()) : 
			?>
			<section class="upcoming">
				<h1>Prochaines dates :</h1>
			<?php
			
			while (have_posts()) : the_post();  
			
			?><article>
				<a class="unstyled" href="<?php the_permalink() ?>">
					<h2 class="hoverable"><?php 
				
				the_title();
				
				// since we're at it, let's also add the ID to the exclude list
				$exclude_id[] = get_the_ID();
				
				 ?></h2>
				 
				 <?php 
				 
				 include( TEMPLATEPATH . '/inc/event-date.php' );
				 
				 if ($event_date != '') { 
				 
				 	echo '<p class="simple"><time itemprop="startDate" datetime="'. esc_attr($start_date_iso) .'">';
				 	echo $event_date;
				 	echo '</time></p>';
				 }
				 
				 // test for LIEUX :
				 				 			
				 			$leslieux = get_the_terms($post->ID, 'lieux' );
				 
				 			if ($leslieux) {
				 			
				 				$lieux_array = array();
				 								 				
				 				foreach($leslieux as $item) {
				 						// add item to array...
				 						$lieux_array[] = array( 
				 								"name" => $item->name, 
				 						    	"url" => get_term_link($item->slug, 'lieux'),
				 						    	"count" => $item->count,
				 						    	"id" => $item->term_id,
				 						   );
				 					}
				 					
				 					?>
				 						  	<p class="lieu simple"><span>Lieu:</span> <?php
				 						   
				 					  		// output everything...
				 					  		foreach ($lieux_array as $key => $row){
				 					  			echo $lieux_array[$key]["name"] ; 	   		
				 					  		}
				 					  		
				 					  		// how many series do we have ?
				 					  		// echo count($series_array);
				 					  		
				 					  		// count items of serie #1
				 					  		$series_item_count = $lieux_array[0]["count"];
				 					  	
				 					  ?></p>
				 					  <?php
				 				
				 			} // end testing for LIEUX.
				 
				  ?>
				  </a>
			</article>
			<?php
			
			endwhile; 
			
			?></section>
			<?php
			
			else : 
	endif;
			
		 ?>
		
		<section class="works">
		
			<?php 
			
			// let's run a loop for the latest of Category
			// "pieces", "performances", "installations, photo...
			// IDs = 3, 18, 20, 37
			
			$core_categories = array(3,20,37 ); 
			
			foreach ($core_categories as $key => $item) {
			  
			  		$custom_query = new WP_Query( array(
			        			//'cat' => '12, 13, 14',
			        					'posts_per_page' => 10,
			        					'meta_key' => '_mem_start_date',
			        					'orderby'  => 'meta_value',
			        					'order'  => 'DESC', // DESC means: newest first
			        					'cat' => $item,
			        					'post__not_in' => $exclude_id,
			        					'tax_query' => array(
			        					  		array(
			        					  			'taxonomy' => 'reglages',
			        					  			'field' => 'slug',
			        					  			'terms' => 'projet-principal',
			        					  		)
			        					  	),
			        			) ); 
			        			
			        			if ($custom_query->have_posts()) : 
			        		?>
			        		<div class="item hlist-item">
			        			<h1 class="hlist-title"><?php 
			        			
			        			echo '<a class="unstyled link" href="'. get_category_link($item) .'">';
			        			
			        			echo get_cat_name($item);
			        			
			        			?></a></h1>
			        		<?php 
			        		while( $custom_query->have_posts() ) : $custom_query->the_post();
			        		
			        				$exclude_id[] = get_the_ID();
			  	      			$has_image = false;
			  	      			include( TEMPLATEPATH . '/inc/featured-img-loop.php' );
			  	      			if ($has_image = true) {
			  	      			
			  	      				echo '<img src="'.$featured_image_url.'"/>';
			  	      			
			  	      				break;
			  	      			}
			  	       	endwhile; ?>
			        		 </div>
			        		 <?php
			        		 
			        		 endif;
			        		 wp_reset_postdata();
			 } // end foreach
				
			 ?>	
			
		</section>

		</div><!-- #content -->
	</section><!-- #primary -->
	
	<section id="footerblocks" class="footerblocks no-bio">
<?php get_sidebar(); ?>
	</section>
<?php get_footer(); ?>