<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package souschiffre
 */
?>

	<div id="footerblock-1" class="widget-area secondary-area shift-right clear" role="complementary">
	
		<?php do_action( 'before_sidebar' ); ?>
		<?php if ( ! dynamic_sidebar( 'footerblock-1' ) ) : ?>

			<aside id="search" class="widget widget_search">
				<?php get_search_form(); ?>
			</aside>

			<aside id="archives" class="widget">
				<h1 class="widget-title"><?php _e( 'Archives', 'souschiffre' ); ?></h1>
				<ul>
					<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
				</ul>
			</aside>

			<aside id="meta" class="widget">
				<h1 class="widget-title"><?php _e( 'Meta', 'souschiffre' ); ?></h1>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</aside>

		<?php endif; 
		
		// end sidebar widget area 
		
		?>
	</div><!-- #secondary -->

	<div id="footerblock-2" class="widget-area secondary-area shift-left clear" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		<?php if ( ! dynamic_sidebar( 'footerblock-2' ) ) : ?>
		<?php endif; 
		
		// end sidebar widget area 
		
		?>
	</div><!-- #secondary -->