<?php
/**
 * @package souschiffre
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

		<div class="entry-meta">
			<?php 
			
			include( TEMPLATEPATH . '/inc/event-date.php' );
				
				if ($event_date != '') { 
					echo $event_date;
				} else {
					souschiffre_posted_on(); 
				}			
			
		?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php //the_content(); ?>
		
		<?php 
		
		// Images:
		
		//image_toolbox( 'vertical-thumb', 1);
		$img_info = img_toolbox_array(); 
		
		if (!empty($img_info)) { 
				
			foreach ($img_info as $key => $row){
//				echo '<img width="'.$img_info[$key]["width-custom"].'" height="'.$img_info[$key]["height-custom"].'" src="'.$img_info[$key]["url-custom"].'" class="attachment-thumbnail" alt="" />';	
			}
		}
		
		// end Images.
		 ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'souschiffre' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->

	
</article><!-- #post-## -->
