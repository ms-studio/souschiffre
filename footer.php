<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package souschiffre
 */
?>

	</div><!-- #main -->
<?php 

/**
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="site-info">
		...
	</div><!-- .site-info -->
</footer><!-- #colophon -->
 */
 
 ?>
</div><!-- #page -->

<?php 
// Javascript at the bottom for fast page loading
//  Grab Google CDN's jQuery. fall back to local if necessary 
//check for the latest version here: https://developers.google.com/speed/libraries/devguide
// Or from http://code.jquery.com/ CDN - provided by (mt) Media Temple

// CDN Speed Test: http://royal.pingdom.com/2012/07/24/best-cdn-for-jquery-in-2012/
// Media Temple CDN is better than Google !!

// code.jquery.com/jquery-1.9.1.min.js
   
 ?>

<?php wp_footer(); ?>

<!-- scripts concatenated and minified via ant build script-->
<script src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>js/mylibs/jquery.colorbox.1.3.34mod.js"></script>
<script src="<?php echo $GLOBALS["TEMPLATE_RELATIVE_URL"] ?>js/script.js?ver=1.24"></script>
<!-- end scripts--> 
  
  


</body>
</html>