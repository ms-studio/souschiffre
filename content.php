<?php
/**
 * @package souschiffre
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'souschiffre' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php 
		
		
		// the_title(); 
		
		$cleantitle = get_the_title();
		$cleantitle = ltrim($cleantitle, ' 0123456789');
		echo $cleantitle;
		
		?></a></h1>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php souschiffre_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'souschiffre' ) ); ?>
		<?php 
		
		// Images:
		
		//image_toolbox( 'vertical-thumb', 1);
		$img_info = img_toolbox_array(); 
		
		if (!empty($img_info)) { 
		
		echo "<p>Images attachées:</p>";
		
			foreach ($img_info as $key => $row){
				echo '<img width="'.$img_info[$key]["width-custom"].'" height="'.$img_info[$key]["height-custom"].'" src="'.$img_info[$key]["url-custom"].'" class="attachment-thumbnail" alt="" />';	
			}
		}
		
		// end Images.
		 ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'souschiffre' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
		<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'souschiffre' ) );
				if ( $categories_list && souschiffre_categorized_blog() ) :
			?>
			<span class="cat-links">
				<?php printf( __( 'Classé sous %1$s', 'souschiffre' ), $categories_list ); ?>
			</span>
			<?php endif; // End if categories ?>

			<?php
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'souschiffre' ) );
				if ( $tags_list ) :
			?>
			<span class="sep"> | </span>
			<span class="tags-links">
				<?php printf( __( 'Mots-clés: %1$s', 'souschiffre' ), $tags_list ); ?>
			</span>
			<?php endif; // End if $tags_list ?>
		<?php endif; // End if 'post' == get_post_type() ?>

		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
		<span class="sep"> | </span>
		<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'souschiffre' ), __( '1 Comment', 'souschiffre' ), __( '% Comments', 'souschiffre' ) ); ?></span>
		<?php endif; ?>

	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
