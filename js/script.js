//$.noConflict();
 
 
jQuery(document).ready(function($) {
// Code that uses jQuery's $ can follow here.
// $(document).ready(function() {	
			
			
			
			$("a[href^=http]").each(
			   function(){ 
			      if(this.href.indexOf(location.hostname) == -1) {
			  			$(this).attr('target', '_blank');
						}
			  	}
			 )

/*
 * CONTENTS
 *
 */
 
 
/* 
 * 0: js-hidden must be hidden - if JS works.
 ****************************************************
 */ 
 $(".js-hidden").hide();

/* 
 * 1.
 * EmailSpamProtection (jQuery Plugin)
 ****************************************************
 * Author: Mike Unckel
 * Description and Demo: http://unckel.de/labs/jquery-plugin-email-spam-protection
 */
$.fn.emailSpamProtection = function(className) {
	return $(this).find("." + className).each(function() {
		var $this = $(this);
		var s = $this.text().replace(" [at] ", "&#64;");
		$this.html("<a href=\"mailto:" + s + "\">" + s + "</a>");
	});
};
//$("body").emailSpamProtection("email");
// note: using .email class = conflicts with Jetpack contact form.

/*
	* show sub-content

*/
$("#alt-content").on("click", "header.sub-header-closed", function(){
// $(".sub-header-closed").click(function() {
		 $(this).removeClass("sub-header-closed");
		 $(this).addClass("sub-header-open");
		 $(this).next(".sub-article").show('200', function() {
		 // Animation complete.	
//		 		scrollTo(document.body.scrollLeft, document.body.scrollTop + 1);
//		 		scrollTo(document.body.scrollLeft, document.body.scrollTop - 1);
		 		// src: http://stackoverflow.com/q/6865310/1904769
		 		// OR:
		 		// src: http://stackoverflow.com/q/16598052/1904769
//		 		$(this).each(function(){
//		 		        var redraw = this.offsetHeight;
//		 		});
		 });
		 return false;
 });

$("#alt-content").on("click", "header.sub-header-open", function(){
// $(".sub-header-closed").click(function() {
		 $(this).removeClass("sub-header-open");
		 $(this).addClass("sub-header-closed");
		 $(this).next(".sub-article").hide('200', function() {
		 // Animation complete.
		 });
		 return false;
 });

/*
 * 2.
 * Vimeo Player
 *
*/

$('.jstrigger-vimeo').click(function(){ // the trigger action . not used anymore...
   var vimeokey = $(this).data('vimeo');
   $(this).replaceWith('<iframe src="http://player.vimeo.com/video/' + vimeokey + '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen class="vimeo-iframe"></iframe>');
   // also: put full title text...
   return false;
 });



/* 
 * 
 * ColorBox (jQuery Plugin)
 ****************************************************
 */
 
$(".colorbox").colorbox({
	rel:'colorbox', 
	transition:"none", 
	width:"95%", 
	height:"97%", 
	fixed:"true",
	
	onLoad:function(){ 
//		$('#cboxClose,#cboxNext,#cboxPrevious').fadeIn(300); 
	// $('#cboxClose').hide();
	$('#cboxPageTitle').hide(); 
		},
	onComplete:function(){ 
	// 	$('#cboxClose').fadeIn(300);
		// $('#cboxPageTitle').show();
//		$('#cboxClose,#cboxNext,#cboxPrevious').delay(900).fadeOut("slow"); 
//		$("#cboxContent").addClass("hidden-controls");
		},
		// end colorbox function
	});


/* 
 * that's it !
 ****************************************************
 */
 				
}); // end document ready
		
		