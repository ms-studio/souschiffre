<?php
/**
 * @package souschiffre
 */
 
// define some variables

$current_post_id = get_the_ID();
$_ef = '<span class="nbsp">&nbsp;</span>';
 

 
?>

<article id="post-<?php echo $current_post_id; ?>" <?php post_class(); ?>>
	
	<?php 
	
	echo '<header class="entry-header';
	
	// test for Featured Image
	
	if ( has_post_thumbnail() ) { 
	// check if the post has a Post Thumbnail assigned to it.
	 echo ' featured-img">';
	 
	  the_post_thumbnail('large'); 
	
	} else {     	
	
			echo '">';
	// image_toolbox('landscape',1); 
		// test if there is a large image attached! 
	} 
	
	 ?>
		<h1 class="entry-title"><?php 
		$regex_name =  souschiffre_title_wrangler();
		echo $regex_name;
//		the_title();
		
		 ?></h1>
		
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>

	</div><!-- .entry-content -->

	
</article><!-- #post-## -->
