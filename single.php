<?php
/**
 * The Template for displaying all single posts.
 *
 * @package souschiffre
 */

//get_header(); 
include( TEMPLATEPATH . '/header.php' );

?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php 
			
			if ( 'post' == get_post_type() ) {
			
//				get_template_part( 'content', 'post' );
				include( TEMPLATEPATH . '/content-post.php' ); 
				
			} else {
			
//				get_template_part( 'content', 'single' ); 
				include( TEMPLATEPATH . '/content-single.php' );
				
			}
						
			?>

			<?php // souschiffre_content_nav( 'nav-below' ); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
			?>

		<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

	<div id="footerblocks" class="footerblocks">
<?php 

		include( TEMPLATEPATH . '/inc/short-bio.php' );
		
		get_sidebar(); 

?>
	</div><!-- #footerblocks -->
<?php get_footer(); ?>