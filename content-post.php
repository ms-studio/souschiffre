<?php
/**
 * 
 * 
 * setup happening in 
 * inc/post-setup.php
 * 
 * @package souschiffre
 */
 
 // note: sub-with-master post status is defined in inc/post-setup.php
 
?>

<article id="post-<?php echo $current_post_id; ?>" <?php post_class($this_post_status); ?>>
	
	<?php 
	
	echo '<header class="entry-header';
	
	// test for Featured Image
	
	if ( has_post_thumbnail() ) { 
	// check if the post has a Post Thumbnail assigned to it.
	 echo ' featured-img">';
	 
	  the_post_thumbnail('large'); 
	
	} else {     	
	
			echo '">';
	// image_toolbox('landscape',1); 
		// test if there is a large image attached! 
	} 
	
	 ?>
		<div class="date-n-title">
		<h1 class="entry-title"><?php 
		
		if ( $this_post_status == 'master' ) {
			
			// output full name:
			the_title();
		
		} else if ( $this_post_status == 'sub-with-master' ) {
				
				$regex_name = souschiffre_title_wrangler();
				
				// return title of master article, with link
				foreach ($filtered_sub_array as $key => $item) { 
						?>
							<a href="<?php echo $item['url']; ?>"><?php 
								echo $item['title']; 
							?></a> &ndash;  
						<?php
					}  // end foreach
				
				echo $regex_name;
		
		} else {
		
					$regex_name = souschiffre_title_wrangler();
					echo $regex_name;
			//		the_title();
		}
		
		 ?></h1>
		
		<?php 
		
		if ( $this_post_status == 'sub-with-master' ) {
		
			// do not show the date
			
		} else {
		
				
							
				if ($event_date != '') { 
				
						echo '<div class="entry-meta">';
						
						echo '<time itemprop="startDate" datetime="'. esc_attr($start_date_iso) .'">';
						
						// is it upcoming ??
						
						if ( $event_is_future == true ) {
								
								echo $event_date;
								
								} else {
							
								// past event!
								echo $event_date_yr;
							
							}
							
						echo '</time>';
						
						echo '</div><!-- .entry-meta -->';
						
				} else { // has no event date
					
							// show "posted on" date.
							// souschiffre_posted_on(); 
							// located in : inc/template-tags.php
				}
			
			
			
			} // end testing for post_status
		
		?>
		</div><!-- .date-n-title -->
	</header><!-- .entry-header -->

	<div class="entry-content">
	
		<div class="entry-content-main">
		<?php the_content(); ?>
		</div>
		
		<div id="alt-content" class="alternate-content">
		
		<?php 
	
		// Has linked pages ?
		
		if ($has_linked_pages == true) {
				
				if ( $this_post_status == 'sub-with-master' ) {
					
					// a sub page with a master page...
					// = do nothing !
										
				} else { 
					
					// display everything !
					echo '<div id="linked-pages" class="linked-pages">';
					
					foreach ($content_sub_array as $key => $item) { 
					
						?>
						<section class="sub-content">
							<header class="sub-header sub-header-closed">
							<h2 class="sub-header-h2"><a href="#<?php // echo $item['url']; ?>"><?php 
								echo $item['title']; 
							?></a></h2>
							</header>
							<article class="js-hidden sub-article sub-article-<?php echo $item['id']; ?>">
							<?php 
								echo $item['content']; 
							?>
							</article>
						</section>
						
						<?php
						
					}  // end foreach
					
					echo '</div>';
				
				}	
					
		} // end Testing for linked pages
		
		// Test for contenu_annexe repeater.
		
		if( have_rows('contenu_annexe') ):
		
		 	// loop through the rows of data
		    while ( have_rows('contenu_annexe') ) : the_row();
		
		        // display a sub field value
		        
		        // annexe_titre
		        // annexe_contenu
		        
		        the_sub_field('sub_field_name');
		        
		        ?>
		        <section class="sub-content">
		        	<header class="sub-header sub-header-closed">
		        	<h2 class="sub-header-h2"><a href="#<?php // echo $item['url']; ?>"><?php 
		        		// echo $item['title']; 
		        		the_sub_field('annexe_titre');
		        	?></a></h2>
		        	</header>
		        	<article class="js-hidden sub-article">
		        	<?php 
		        		// echo $item['content']; 
		        		the_sub_field('annexe_contenu');
		        	?>
		        	</article>
		        </section>
		        
		        <?php
		        
		    endwhile;
		
		else :
		    // no rows found
		endif;
		
		
		// Images:
		// ********************
		
		$has_gallery = false;
		
		$galeries_photos =  get_post_meta($post->ID, 'bloc_galeries_photos', false);
				
		if ($galeries_photos) {
		
				// tester aussi si $galeries_photos[0] = zero
				
				if  ( $galeries_photos[0] > 0) {
					
					$has_gallery = true;
					
//					echo '<pre class="hidden admin-visible">';
//					var_dump($galeries_photos);
//					echo '</pre>';
					
					$img_info = gallery_toolbox(); // in : functions-custom.php
					
				} 
						
		}
		
		if ($has_gallery == false) { 
						
						// Sinon : méthode classique
						// Chercher les images attachées à l'article.
						
						$img_info = img_toolbox_array(); 
						
												
			} // end Méthode classique
			
			
//								echo '<pre class="hidden admin-visible">';
//								var_dump($img_info);
//								echo '</pre>';
			
			
			if (!empty($img_info)) { 
			
			// new system: every array = one gallery!
			
//			echo '<pre class="hidden admin-visible">';
//			var_dump($img_info);
//			echo '</pre>';
			
			foreach ($img_info as $key => $gallery){ 
						
						$gallery_title = $gallery["title"];
						$gallery_description = $gallery["descriptions"];
						$gallery_images = $gallery["images"];
						
						
				?>
					<section class="sub-content img-gallery">
						<header class="sub-header sub-header-closed">
							<h2 class="sub-header-h2"><a href="#" class="unstyled2"><span class="hoverable2"><?php 
							
							if (!empty($gallery_title)) {
							
									echo $gallery_title;
									
							} else {
							
								echo 'Images';
								
								if ( is_user_logged_in() ) {
										echo '*';
										// so we know if ACF gallery is not used.
								}
								
							}
							
							 ?></span> <?php
							
									foreach ($gallery_images as $key => $item){
										echo '<img width="'.$item["width-custom"].'" height="'.$item["height-custom"].'" src="'.$item["url-custom"].'" class="attachment-thumbnail gallery-preview" alt="" />';	
									} // end Foreach $img_info
									
									?>
							</a></h2>
							
						</header>
						<div class="js-hidden sub-article sub-article-img clear">
					<?php
				
						foreach ($gallery_images as $key => $item){
							echo '<a href="'.$item["url-large"].'" class="colorbox img-gl-itm" data-caption="'.esc_attr($item["caption"]).'"><img width="'.$item["width-custom"].'" height="'.$item["height-custom"].'" src="'.$item["url-custom"].'" class="attachment-thumbnail" alt="" /></a>';	
						} // end Foreach $img_info
						
						?>
						</div>
					</section>
						<?php
				
				
			} // end $img_info
				
			} // end if !empty $img_info
			
		// end Images.
		
		// **************************************************************
		
		// Test for video:
		
		$vimeolinks = get_post_meta($post->ID, 'lien_vimeo', false);
		
		if ( $vimeolinks ) {
		
		// var_dump($vimeolinks);
		
		if  ( $vimeolinks[0] != '') {
		
		$vimeo_all_small = 'false';
		
		// method 2: build an array
		
		$vimeodata  = array();
		
		?>
		<section class="sub-content video-gallery">
			<header class="sub-header sub-header-closed">
				<h2 class="sub-header-h2"><a href="#">Vidéo</a></h2>
			</header>
			<div class="js-hidden sub-article sub-article-img sans-retrait">
		<?php
				
		 foreach($vimeolinks as $vimeolink) {
						
						// test also for the TYPE of vimeo link (album, channel...)
						// echo 'this: '.$vimeolink;
									
									// clean the URL:
											$vimeolink = str_replace("http://vimeo.com/", "", $vimeolink);
											$vimeolink = str_replace("https://vimeo.com/", "", $vimeolink);
											$vimeolink = str_replace("vimeo.com/", "", $vimeolink);
											$vimeolink = 'http://vimeo.com/'.$vimeolink;
									// $vimeolink = rtrim($vimeolink,"/");
									
										 $vimeo_test = discoverVimeo($vimeolink);
										 // in functions-custom.php
										 $vimeo_type = $vimeo_test["type"];
										 $vimeo_id = $vimeo_test["id"];
									
									if ($vimeo_id != '') {
										include( TEMPLATEPATH . '/inc/vimeo-embed.php' );
									}		 
		
				 }  // end Foreach
			
		?>
			</div>
		</section>
			<?php
		
			}
		} // end Vimeolinks
		
		 ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'souschiffre' ), 'after' => '</div>' ) ); 
		?>
		
		</div><!-- #alt-content -->
	
	</div><!-- .entry-content -->
	
	<?php
	
	if ( $this_post_status !== 'sub-with-master' ) { // show lieux + categories
	
	?>
	
	<footer class="entry-footer">
		
		<?php
		
		echo '<div class="rel tagcloud">';
		
			$category_list = get_the_category_list( __( ' ', 'souschiffre' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ' ', 'souschiffre' ) );

			if ( '' != $tag_list ) {
				$meta_text = __( '<div class="categories">%1$s</div> <div class="keywords">%2$s</div>', 'souschiffre' );
			} else {
				$meta_text = __( '<div class="categories">%1$s</div>', 'souschiffre' );
			}
			
			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);
			
			echo '</div>';
		
		?>
		
			<?php
					
					/*
					* TEST LIEUX
					* vérifier si une 
					*/
								
		//			$posttags = get_the_tags();
					
					$leslieux = get_the_terms($post->ID, 'lieux' );
		
					if ($leslieux) {
					
						$lieux_array = array();
						
						// echo count($lieux_array);
						
						foreach($leslieux as $item) {
								// add item to array...
								$lieux_array[] = array( 
										"name" => $item->name, 
								    	"url" => get_term_link($item->slug, 'lieux'),
								    	"count" => $item->count,
								    	"id" => $item->term_id,
								    	"descr" => $item->description,
								   );
							}
							
							// how many Lieux do we have ?
							// echo count($series_array);
							
							// count items of serie #1
							$lieux_count = count($lieux_array); // $lieux_array[0]["count"];
							
							
							if ( $event_is_future == true ) { 
							
									?>
										 <div class="rel liste-lieux">
										  	<h3 class="h3"><?php
										  	
										  	if ($lieux_count > 1) {
										  		echo 'Lieux: ';
										  	} else {
										  		echo 'Lieu: ';
										  	}
										  	
										  	echo '</h3>';
										   
									  		// output everything...
									  		foreach ($lieux_array as $key => $item){
									  		
									  			echo '<h4>'.$item["name"].'</h4>';
									  			
									  			$tag_description = $item["descr"];
									  					if ( ! empty( $tag_description ) ) {
									  					
									  					$tag_description = apply_filters('the_content', $tag_description);					
		  												$ret = ' ' . $tag_description;
		  												$attribs = ''; 
		  												$ret = preg_replace(
		  													array(
		  														'#([\s>])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is',
		  														'#([\s>])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is',
		  														'#([\s>])([a-z0-9\-_.]+)@([^,< \n\r]+)#i'),
		  													array(
		  														'$1<a href="$2"' . $attribs . '>$2</a>',
		  														'$1<a href="http://$2"' . $attribs . '>$2</a>',
		  														'$1<a href="mailto:$2@$3">$2@$3</a>'),$ret);
		  												$ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
		  												$ret = trim($ret);
		  												echo '<div class="taxonomy-description">' . $ret . '</div>';
									  						}
									  		} // end foreach 
									  	
									  ?>
									   </div><!-- #lieux -->
									  <?php
							
							} else {
							
							?>
								 <div class="rel lieux-actifs tagcloud">
								  	
								  	<?php 
		
							  		// output everything...
							  		foreach ($lieux_array as $key => $row){
							  		
							  			echo '<a href="'. $lieux_array[$key]["url"] .'">' ;
							  			echo $lieux_array[$key]["name"].'</a>' ; 	   		
							  		
							  		}
							  	
							  ?>
							   </div>
							  <?php
							  
							  }
						
					} // end testing for LIEUX.
					
					?>

	</footer><!-- .entry-meta -->
	<?php 
	
			} // end post_status test
	
	 ?>
	
</article><!-- #post-## -->
