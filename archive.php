<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package souschiffre
 */

get_header(); 


// init values:
$exclude_id = array();
$archive_array = array();
$page_has_content = false;

// Display everything, pagination sucks...

query_posts( $query_string . '&posts_per_page=-1' ); // &post_type=any

?>
<!-- archive.php -->

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php 
				
		if ( have_posts() ) : 
		
			$page_has_content = true;
		
		?>

			<header class="page-header">
				<h1 class="page-title">
					<?php
						if ( is_category() ) {
							// printf( __( 'Catégorie: %s', 'souschiffre' ), '<span>' . single_cat_title( '', false ) . '</span>' );

						} elseif ( is_tag() ) {
							printf( __( 'Mot-clef: %s', 'souschiffre' ), '<span>' . single_tag_title( '', false ) . '</span>' );
							
							} elseif ( is_tax() ) {
							
								if ( is_tax( 'lieux' ) ) {
									$tax_name = "";
								} else {
									$tax_name = "Archives pour: ";
								}
								
								echo $tax_name;
								echo wptexturize( get_queried_object()->name ) ;
							

						} elseif ( is_author() ) {
							/* Queue the first post, that way we know
							 * what author we're dealing with (if that is the case).
							*/
							the_post();
							printf( __( 'Author Archives: %s', 'souschiffre' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' );
							/* Since we called the_post() above, we need to
							 * rewind the loop back to the beginning that way
							 * we can run the loop properly, in full.
							 */
							rewind_posts();

						} elseif ( is_day() ) {
							printf( __( 'Daily Archives: %s', 'souschiffre' ), '<span>' . get_the_date() . '</span>' );

						} elseif ( is_month() ) {
							printf( __( 'Monthly Archives: %s', 'souschiffre' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

						} elseif ( is_year() ) {
							printf( __( 'Yearly Archives: %s', 'souschiffre' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

						} else {
							_e( 'Archives', 'souschiffre' );

						}
					?>
				</h1>
				<?php
					if ( is_category() ) {
						// show an optional category description
						$category_description = category_description();
						if ( ! empty( $category_description ) )
							echo apply_filters( 'category_archive_meta', '<div class="taxonomy-description">' . $category_description . '</div>' );

					} elseif ( is_tag() || is_tax() ) {
						// show an optional tag description
						
//						echo 'The post type is: '.get_post_type( get_the_ID() );
						
						$tag_description = tag_description();
						if ( ! empty( $tag_description ) ) {
//							echo apply_filters( 'tag_archive_meta', '<div class="taxonomy-description">' . $tag_description . '</div>' );
							
									$ret = ' ' . $tag_description;
									$attribs = ''; 
									$ret = preg_replace(
										array(
											'#([\s>])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is',
											'#([\s>])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is',
											'#([\s>])([a-z0-9\-_.]+)@([^,< \n\r]+)#i'),
										array(
											'$1<a href="$2"' . $attribs . '>$2</a>',
											'$1<a href="http://$2"' . $attribs . '>$2</a>',
											'$1<a href="mailto:$2@$3">$2@$3</a>'),$ret);
									$ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
									$ret = trim($ret);
									echo '<div class="taxonomy-description">' . $ret . '</div>';
							
							} else {
							//echo 'no description';
							}
					}  
				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */						
			 ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					
					if (in_array( get_the_ID(), $exclude_id )) {
					    // is a duplicate
					} else {
					
						include( TEMPLATEPATH . '/inc/archive-array.php' );
						
					}
					
				?>

			<?php endwhile;
			
			
					// test the array:
								
//								echo '<div class="">';
//								var_dump($archive_array);
//								echo '</div>';
								
								// ok, the $archive_array has been built.
								// now, sort it by DATE:
								
								function multi_array_sort($a,$b) {
								     return $a['pubdate']<$b['pubdate'];
								}
								usort($archive_array, "multi_array_sort");
								
								// and now, loop through the array:
								
								foreach ($archive_array as $key => $item) { 
								
									// include( TEMPLATEPATH . '/inc/archive-output.php' );
									include( TEMPLATEPATH . '/inc/archive-item.php' );
									
								} // end Foreach loop
			
			 ?>

			<?php souschiffre_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->
	<section id="footerblocks" class="footerblocks">
<?php 

include( TEMPLATEPATH . '/inc/short-bio.php' );

get_sidebar(); 

?>
	</section>
<?php get_footer(); ?>